# MAlang


## TODO



## Syscall translation
Mac accepts the unix syscalls. They only need to be translated to the mac syscalls.

[Construct Syscalls](https://opensource.apple.com/source/xnu/xnu-792.13.8/osfmk/mach/i386/syscall_sw.h#l128)
```c
#define SYSCALL_CLASS_SHIFT	24
#define SYSCALL_CLASS_MASK	(0xFF << SYSCALL_CLASS_SHIFT)
#define SYSCALL_NUMBER_MASK	(~SYSCALL_CLASS_MASK)
 
#define SYSCALL_CLASS_UNIX	2	/* Unix/BSD */
 
/* Macros to simpllfy constructing syscall numbers. */
#define SYSCALL_CONSTRUCT_UNIX(syscall_number) \
			((SYSCALL_CLASS_UNIX << SYSCALL_CLASS_SHIFT) | \
			 (SYSCALL_NUMBER_MASK & (syscall_number)))
 
```
Basically the unix syscalls translate like this:
```
(0×2000000 + unix syscall #)
```

## Syntax
```mermaid
stateDiagram-v2
    [*] --> Still
    Still --> [*]

    Still --> Moving
    Moving --> Still
    Moving --> Crash
    Crash --> [*]
```

## Keywords

## TODO
- [ ] Compiled
- [ ] Turing Complete
- [ ] Self Hosted
- [ ] Dead Code Elimination
- [ ] Crossplatform
