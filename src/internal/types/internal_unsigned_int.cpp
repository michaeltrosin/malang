//
// Created by Ra6nar0k21 on 11.02.22.
//

#include "internal_unsigned_int.h"

#include <asserting.h>

InternalUnsignedInt::InternalUnsignedInt() : _uint() {}
InternalUnsignedInt::InternalUnsignedInt(uint32_t initialValue) : _uint(initialValue) {}

std::any InternalUnsignedInt::getRaw() { return this->_uint; }

int32_t InternalUnsignedInt::asNumber() { return (int32_t) this->_uint; }

std::string InternalUnsignedInt::asString() { return std::to_string(this->_uint); }
ValueType InternalUnsignedInt::getType() const { return ValueType::eUINT; }

