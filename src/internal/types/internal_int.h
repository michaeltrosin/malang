//
// Created by Ra6nar0k21 on 11.02.22.
//

#ifndef MALANG_SRC_INTERNAL_TYPES_INTERNAL_INT_H_
#define MALANG_SRC_INTERNAL_TYPES_INTERNAL_INT_H_

#include "value.h"

class InternalInt : public InternalValue {
public:
  InternalInt();
  InternalInt(int32_t initialValue);

  std::any getRaw() override;
  int32_t asNumber() override;
  std::string asString() override;
  ValueType getType() const override;

private:
  int32_t _int;
};

#endif //MALANG_SRC_INTERNAL_TYPES_INTERNAL_INT_H_
