//
// Created by Ra6nar0k21 on 11.02.22.
//

#include "internal_void.h"

#include <asserting.h>

InternalVoid::InternalVoid() = default;

std::any InternalVoid::getRaw() { return 0; }

int32_t InternalVoid::asNumber() { return 0; }

std::string InternalVoid::asString() { return ""; }
ValueType InternalVoid::getType() const { return ValueType::eVOID; }
