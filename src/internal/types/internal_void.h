//
// Created by Ra6nar0k21 on 11.02.22.
//

#ifndef MALANG_SRC_INTERNAL_TYPES_INTERNAL_VOID_H_
#define MALANG_SRC_INTERNAL_TYPES_INTERNAL_VOID_H_

#include "value.h"

class InternalVoid: public InternalValue {
public:
  InternalVoid();

  std::any getRaw() override;
  int32_t asNumber() override;
  std::string asString() override;
  ValueType getType() const override;
};

#endif //MALANG_SRC_INTERNAL_TYPES_INTERNAL_VOID_H_
