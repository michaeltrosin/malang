//
// Created by Ra6nar0k21 on 11.02.22.
//

#include "internal_string.h"

#include <asserting.h>

InternalString::InternalString() : _stringValue() {}
InternalString::InternalString(std::string initialValue) : _stringValue(std::move(initialValue)) {}

std::any InternalString::getRaw() { return this->_stringValue; }

int32_t InternalString::asNumber() {
  assert_not_reached("String cannot be cast to number");
  return 0;
}

std::string InternalString::asString() { return this->_stringValue; }
ValueType InternalString::getType() const { return ValueType::eSTRING; }
