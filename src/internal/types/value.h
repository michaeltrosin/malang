//
// Created by Ra6nar0k21 on 10.02.22.
//

#ifndef MALANG_SRC_INTERNAL_TYPES_VALUE_H_
#define MALANG_SRC_INTERNAL_TYPES_VALUE_H_

#include <any>
#include <string>

#include "internal_types.h"

class InternalValue {
public:
  virtual std::any getRaw() = 0;

  virtual int32_t asNumber() = 0;
  virtual std::string asString() = 0;

  virtual ValueType getType() const = 0;
private:

};

#endif //MALANG_SRC_INTERNAL_TYPES_VALUE_H_
