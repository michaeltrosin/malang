//
// Created by Ra6nar0k21 on 09.02.22.
//

#include "lexer.h"
#include <asserting.h>

#include <sstream>
#include <fstream>
#include <map>

const std::map<std::string, TokenType> kKEYWORDS = {
  {"pub", TokenType::ePUBLIC},
  {"prv", TokenType::ePRIVATE},
  {"readonly", TokenType::eREADONLY},

  {"let", TokenType::eLET},
  {"met", TokenType::eMET},

  {"if", TokenType::eIF},
  {"else", TokenType::eELSE},

  {"while", TokenType::eWHILE},
  {"for", TokenType::eFOR},
  {"do", TokenType::eDO},
  {"break", TokenType::eBREAK},
  {"continue", TokenType::eCONTINUE},
  {"return", TokenType::eRETURN},

  {"use", TokenType::eUSE},
  {"match", TokenType::eMATCH},
  {"switch", TokenType::eSWITCH},
  {"case", TokenType::eCASE},
  {"default", TokenType::eDEFAULT},

  {"class", TokenType::eCLASS},
  {"interface", TokenType::eINTERFACE},
  {"abstract", TokenType::eABSTRACT},

  {"new", TokenType::eNEW},
  {"expose", TokenType::eEXPOSE},
};

const std::map<std::string, TokenType> kOPERATORS = {
  {"+", TokenType::ePLUS},
  {"-", TokenType::eMINUS},
  {"*", TokenType::eSTAR},
  {"/", TokenType::eSLASH},
  {"%", TokenType::ePERCENT},
  {"@", TokenType::eAT},
  {"=", TokenType::eEQ},
  {"==", TokenType::eEQEQ},
  {"!", TokenType::eEXCL},
  {"!=", TokenType::eEXCLEQ},
  {"<=", TokenType::eLTEQ},
  {">=", TokenType::eGTEQ},
  {"<", TokenType::eLT},
  {">", TokenType::eGT},
  {"+=", TokenType::ePLUSEQ},
  {"-=", TokenType::eMINUSEQ},
  {"*=", TokenType::eSTAREQ},
  {"/=", TokenType::eSLASHEQ},
  {"%=", TokenType::ePERCENTEQ},
  {"&=", TokenType::eAMPEQ},
  {"^=", TokenType::eCARETEQ},
  {"|=", TokenType::eBAREQ},
  {"<<=", TokenType::eLTLTEQ},
  {">>=", TokenType::eGTGTEQ},
  {">>>=", TokenType::eGTGTGTEQ},
  {"++", TokenType::ePLUSPLUS},
  {"--", TokenType::eMINUSMINUS},
  {"<<", TokenType::eLTLT},
  {">>", TokenType::eGTGT},
  {">>>", TokenType::eGTGTGT},
  {"**", TokenType::eSTARTSAR},
  {"?:", TokenType::eQUESTIONCOLON},
  {"??", TokenType::eQUESTIONQUESTION},
  {"~", TokenType::eTILDE},
  {"^", TokenType::eCARET},
  {"|", TokenType::eBAR},
  {"||", TokenType::eBARBAR},
  {"&", TokenType::eAMP},
  {"&&", TokenType::eAMPAMP},
  {"?", TokenType::eQUESTION},
  {":", TokenType::eCOLON},
  {"::", TokenType::eCOLONCOLON},
  {";", TokenType::eSEMICOLON},
  {"(", TokenType::eLPAREN},
  {")", TokenType::eRPAREN},
  {"[", TokenType::eLBRACKET},
  {"]", TokenType::eRBRACKET},
  {"{", TokenType::eLBRACE},
  {"}", TokenType::eRBRACE},
  {",", TokenType::eCOMMA},
  {".", TokenType::eDOT},
};

bool isHexNumber(char current) {
  return (current >= 'a' && current <= 'f') ||
    (current >= 'A' && current <= 'F') ||
    (current >= '0' && current <= '9');
}

bool isBinNumber(char current) {
  return (current >= '0' && current <= '1');
}

bool isOctNumber(char current) {
  return (current >= '0' && current <= '7');
}

std::string kOPERATOR_CHARS("+-*/%()[]{}=<>!&|.,^~?:;");

Lexer::Lexer(const std::string &programPath) : _strBuf() {
  std::ifstream file(programPath);

  // if the file exists, open it, read all data to _fileContent
  if (file) {
    std::ostringstream ss;
    ss << file.rdbuf();
    this->_fileContent = ss.str();
    this->_fileLength = this->_fileContent.length();
  }
}

std::vector<Token> Lexer::tokenize() {
  while (this->_pos < this->_fileLength) {
    auto current = this->peek(0);

    // TODO: addStatement strings
    // TODO: addStatement boolean

    if (Lexer::isIdentifierStart(current)) {
      this->tokenizeIdentifier();
    } else if (isnumber(current)) {
      this->tokenizeNumber();
    } else if (current == '#') {
      this->next();
      this->tokenizeHexNumber();
    } else if (kOPERATOR_CHARS.find(current) != std::string::npos) {
      this->tokenizeOperator();
    } else {
      current = this->next();
    }
  }
  return this->_tokens;
}

void Lexer::tokenizeNumber() {
  this->clearBuffer();
  auto current = this->peek(0);
  if (current == '0') {
    auto next = this->peek(1);
    if (next == 'x' || next == 'X') {
      this->next();
      this->next();
      this->tokenizeHexNumber();
      return;
    } else if (next == 'b' || next == 'B') {
      this->next();
      this->next();
      this->tokenizeBinNumber();
      return;
    } else if (next > '0' && next <= '7') {
      this->next();
      this->tokenizeOctNumber();
      return;
    }
  }

  while (true) {
    if (current == '.') {
      if (this->_strBuf.find('.') != std::string::npos) {
        assert_not_reached("Invalid float number");
      }
    } else if (!isdigit(current) && current != '_') {
      // Int/Float can contain _ spacers
      break;
    }
    this->_strBuf += current;
    current = this->next();
  }
  this->addToken(TokenType::eNUMBER, this->_strBuf);
}

void Lexer::tokenizeBinNumber() {
  this->clearBuffer();
  char current = this->peek(0);
  // Binary can contain _ spacers
  while (isBinNumber(current) || (current == '_')) {
    if (current != '_') {
      // allow _ symbol
      this->_strBuf += current;
    }
    current = this->next();
  }
  auto length = this->_strBuf.length();
  if (length > 0) {
    this->addToken(TokenType::eNUMBER_BIN, this->_strBuf);
  }
}

void Lexer::tokenizeHexNumber() {
  this->clearBuffer();
  char current = this->peek(0);
  // Hex can contain _ spacers
  while (isHexNumber(current) || (current == '_')) {
    if (current != '_') {
      // allow _ symbol
      this->_strBuf += current;
    }
    current = this->next();
  }
  auto length = this->_strBuf.length();
  if (length > 0) {
    this->addToken(TokenType::eNUMBER_HEX, this->_strBuf);
  }
}

void Lexer::tokenizeOctNumber() {
  this->clearBuffer();
  char current = this->peek(0);
  // Octal can contain _ spacers
  while (isOctNumber(current) || (current == '_')) {
    if (current != '_') {
      // allow _ symbol
      this->_strBuf += current;
    }
    current = this->next();
  }
  auto length = this->_strBuf.length();
  if (length > 0) {
    this->addToken(TokenType::eNUMBER_OCT, this->_strBuf);
  }
}

void Lexer::tokenizeIdentifier() {
  this->clearBuffer();
  this->_strBuf += this->peek(0);
  auto current = this->next();

  while (true) {
    if (!Lexer::isIdentifierPart(current)) {
      break;
    }

    this->_strBuf += current;
    current = this->next();
  }

  std::string identifier = this->_strBuf;
  // if the identifier is a keyword
  if (kKEYWORDS.find(identifier) != kKEYWORDS.end()) {
    this->addToken(kKEYWORDS.find(identifier)->second);
  } else {
    this->addToken(TokenType::eIDENTIFIER, identifier);
  }
}

bool Lexer::isIdentifierStart(char current) {
  return (current >= 'a' && current <= 'z') ||
    (current >= 'A' && current <= 'Z') ||
    current == '_' ||
    current == '$';
}

bool Lexer::isIdentifierPart(char current) {
  return (current >= '0' && current <= '9') ||
    (current >= 'A' && current <= 'Z') ||
    (current >= 'a' && current <= 'z') ||
    current == '_' ||
    current == '$';
}

void Lexer::tokenizeOperator() {
  auto current = peek(0);
  if (current == '/') {
    if (this->peek(1) == '/') {
      this->next();
      this->next();
      this->tokenizeComment(false);
      return;
    } else if (this->peek(1) == '*') {
      this->next();
      this->next();
      this->tokenizeComment(true);
      return;
    }
  }

  this->clearBuffer();

  while (true) {
    std::string text = this->_strBuf;
    if (!text.empty() && kOPERATORS.find(text + current) == kOPERATORS.end()) {
      this->addToken(kOPERATORS.find(text)->second);
      return;
    }
    this->_strBuf += current;
    current = this->next();
  }
}

void Lexer::tokenizeComment(bool multiline) {
  auto current = this->peek(0);
  if (multiline) {
    while (true) {
      if (current == '*' && this->peek(1) == '/') break;
      if (current == '\0') {
        assert_not_reached("Reached end of file while parsing multiline comment");
      }
      current = this->next();
    }
    this->next(); // *
    this->next(); // /
  } else {
    std::string newLine("\r\n");
    while (newLine.find(current) == std::string::npos) {
      current = this->next();
    }
  }
}

void Lexer::clearBuffer() {
  this->_strBuf.clear();
}

char Lexer::next() {
  this->_pos++;
  auto result = this->peek(0);
  // TODO: Line and Column numbers
  return result;
}

char Lexer::peek(int offset) const {
  auto pos = this->_pos + offset;
  if (pos >= this->_fileLength) { return '\0'; }
  return this->_fileContent[pos];
}

void Lexer::addToken(TokenType type) { this->addToken(type, ""); }

void Lexer::addToken(TokenType type, const std::string &text) {
  this->_tokens.emplace_back(type, text, this->_row, this->_column);
}
