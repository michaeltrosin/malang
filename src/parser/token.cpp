//
// Created by Ra6nar0k21 on 09.02.22.
//

#include "token.h"

#include <utility>

Token::Token(TokenType type, std::string text, uint32_t row, uint32_t column) : _type(type), _text(std::move(text)), _row(row), _column(column) {}
