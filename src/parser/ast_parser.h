//
// Created by Ra6nar0k21 on 11.02.22.
//

#ifndef MALANG_SRC_PARSER_AST_PARSER_H_
#define MALANG_SRC_PARSER_AST_PARSER_H_

#include "token.h"

#include <vector>
#include <ast/fwd_stmt.h>
#include <ast/fwd_expr.h>

class ASTParser {
public:
  explicit ASTParser(const std::vector<Token>& tokens);

  std::shared_ptr<ProgramStmt> parseProgram(const std::string& fileName);

  std::shared_ptr<Stmt> parseStmt();
  std::shared_ptr<UseStmt> parseUseStmt();

  std::shared_ptr<Expr> parseExpr();
private:
  Token consume(TokenType type);
  bool matches(TokenType type);
  bool peekMatches(TokenType type, uint32_t offset);
  Token get(uint32_t offset);

private:
  std::vector<Token> _tokens;
  uint32_t _size;
  uint32_t _position;
};

#endif //MALANG_SRC_PARSER_AST_PARSER_H_
