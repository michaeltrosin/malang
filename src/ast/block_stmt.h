//
// Created by Ra6nar0k21 on 12.02.22.
//

#ifndef MALANG_SRC_AST_BLOCK_STMT_H_
#define MALANG_SRC_AST_BLOCK_STMT_H_

#include <vector>
#include "ast_node.h"

class BlockStmt : public Stmt {
public:
  void accept(Visitor &visitor) override {}

  void addStatement(const std::shared_ptr<Stmt> &statement) {
    this->_statements.emplace_back(statement);
  }
private:
  std::vector<std::shared_ptr<Stmt>> _statements;
};

#endif //MALANG_SRC_AST_BLOCK_STMT_H_
