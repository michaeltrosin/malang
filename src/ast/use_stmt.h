//
// Created by Ra6nar0k21 on 12.02.22.
//

#ifndef MALANG_SRC_AST_USE_STMT_H_
#define MALANG_SRC_AST_USE_STMT_H_

#include "ast_node.h"
#include <ast/fwd_expr.h>
#include <memory>

class UseStmt : public Stmt {
public:
  explicit UseStmt(const std::shared_ptr<Expr> &useExpression) : _useExpression(useExpression) {}

  void accept(Visitor &visitor) override {

  }

private:
  std::shared_ptr<Expr> _useExpression;
};

#endif //MALANG_SRC_AST_USE_STMT_H_
