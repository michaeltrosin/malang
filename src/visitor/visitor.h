//
// Created by Ra6nar0k21 on 10.02.22.
//

#ifndef MALANG_SRC_VISITORS_VISITOR_H_
#define MALANG_SRC_VISITORS_VISITOR_H_

#include <ast/fwd_stmt.h>
#include <ast/fwd_expr.h>

class Visitor {
public:
  virtual void visit(std::shared_ptr<ProgramStmt> program) = 0;
  virtual void visit(std::shared_ptr<BlockStmt> block) = 0;
  virtual void visit(std::shared_ptr<UseStmt> use) = 0;
};

#endif //MALANG_SRC_VISITORS_VISITOR_H_
