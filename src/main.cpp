//
// Created by Ra6nar0k21 on 09.02.22.
//

#include <iostream>
#include <chrono>
#include <argparse.h>
#include <parser/lexer.h>
#include <parser/ast_parser.h>

int main(int argc, const char *argv[]) {
  using std::chrono::high_resolution_clock;
  using std::chrono::duration_cast;
  using std::chrono::duration;
  using std::chrono::milliseconds;

  argparse::ArgumentParser argument_parser("malang", "");

  // The path to the program file with the met main inside
  argument_parser.add_argument().names({"-p", "--program"}).description("Sets the program file").required(true);

  // Compiles the program to a runnable binary
  argument_parser.add_argument().names({"-c", "--compile"}).description("Compiles the program").required(false);
  // Interprets the program
  argument_parser.add_argument().names({"-r", "--run"}).description("Runs the program").required(false);

  // Files to be added to the parsing include path
  argument_parser.add_argument().names({"-i", "--include"}).description("Sets the include path").required(false);

  argument_parser.enable_help();
  auto err = argument_parser.parse(argc, argv);
  if (err) {
    std::cout << err << std::endl;
    return -1;
  }

  if (argument_parser.exists("help")) {
    argument_parser.print_help();
    return 0;
  }

  // if (argument_parser.exists("include")) {
  // }

  std::string programPath;

  if (argument_parser.exists("program")) {
    programPath = argument_parser.get<std::string>("program");
  }

  // No program path specified
  if (programPath.empty()) { return -1; }

  auto programName = programPath;
  // Remove the path
  programName = programName.substr(programPath.find_last_of('/') + 1);
  // Remove file ending
  programName = programName.substr(0, programName.find_last_of('.'));

  auto t1 = high_resolution_clock::now();
  Lexer lexer(programPath);
  auto t2 = high_resolution_clock::now();
  /* Getting number of milliseconds as a double. */
  duration<double, std::milli> ms_double = t2 - t1;
  std::cout << "Lexing took " << ms_double.count() << " ms" << std::endl;

  t1 = high_resolution_clock::now();
  ASTParser parser(lexer.tokenize());
  auto program = parser.parseProgram(programName);
  t2 = high_resolution_clock::now();
  /* Getting number of milliseconds as a double. */
  ms_double = t2 - t1;
  std::cout << "Parsing took " << ms_double.count() << " ms" << std::endl;

  /*
    auto tokens = lexer.tokenize();
    for (const auto &token : tokens) {
      std::cout << token.toString() << std::endl;
    }
  */
  return 0;
}
